# Dz03_02

Potrebno je napisati klasu `MatImpl` koja implementira interfejs `IMat`.
```java
public interface IMat {

	default public int calc(String sentence) throws BadSentenceException {
		return 0;
	}
}
```

## Uraditi
* Metoda `public int calc(String sentence)` kao parametar pri rečenicu koja predstavlja matematički izraz.
Primer rečenice "dva plus dva". Potrebno je izračunati vrednost izraza i to vratiti kao celobrojnu vrednost.
* Napisati klasu izuzetka BadSentenceException.
Ovaj izuzetak se baca u slučaju kada ne može da se protumači dobijena rečenica.

## Pretpostavke
Ulazni parametar - rečenica može da sadrži sledeće reči
* Pozitivne brojeve od nula do dvadeset
* Operacije plus, minus, puta, podeljeno i ostatak
* Rečenica sadrži isključivo reči
* Puta, podeljeno i ostatak imaju veći prioritet od plus i minus

